## v1.0.2

1. 适配API9

2. 直接引用源库使用，删除本库的绝大部分逻辑，仅将客制化的部分抽出作为lib

## v1.0.1

- 已实现功能
  1. sha224
  2. sha384
  3. sha3
  4. hmac-sha224
  5. hmac-sha384
  6. hmac-sha3
  7. pbkdf2
  8. aes
  9. tripledes
  10. rc4
  11. rabbit
  12. rabbit-legacy
  13. evpkdf
  14. format-openssl
  15. format-hex
  16. enc-latin1
  17. enc-utf8
  18. enc-hex
  19. enc-utf16
  20. enc-base64
  21. mode-cfb
  22. mode-ctr
  23. mode-ctr-gladman
  24. mode-ofb
  25. mode-ecb
  26. pad-pkcs7
  27. pad-ansix923
  28. pad-iso10126
  29. pad-iso97971
  30. pad-zeropadding
  31. pad-nopadding

## v1.0.0

- 已实现功能
  1. md5
  2. sha1
  3. sha256
  4. sha512
  5. ripemd160
  6. hmac-md5
  7. hmac-sha1
  8. hmac-sha256
  9. hmac-sha512
  10. hmac-ripemd160