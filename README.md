# crypto-js

## 简介
> crypto-js是一个加密算法类库，可以非常方便的在前端进行其所支持的加解密操作。目前crypto-js已支持的算法有：MD5、SHA-1、SHA-256、HMAC、HMAC-MD5、HMAC-SHA1、HMAC-SHA256、PBKDF2等。

![preview.gif](preview/preview.gif)

## 下载安装
```shell
npm install @ohos/crypto-js --save
```
OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明
1. 引入依赖
 ```
   import { CryptoJS } from '@ohos/crypto-js'
 ```
2. md5
 ```
   CryptoJS.MD5('message')
 ```
3. sha1
 ```
   CryptoJS.SHA1('message');
 ```
4. sha256
 ```
   CryptoJS.SHA256('message')
 ```
5. sha512
 ```
   CryptoJS.SHA512('message')
 ```
6. ripemd160
 ```
   CryptoJS.RIPEMD160('message')
 ```
7. hmac-md5
 ```
   CryptoJS.HmacMD5('message', 'pwd')
 ```
8. hmac-sha1
 ```
   CryptoJS.HmacSHA1('message', 'pwd')
 ```
9. hmac-sha256
 ```
   CryptoJS.HmacSHA256('message', 'pwd')
 ```
10. hmac-sha512
 ```
   CryptoJS.HmacSHA512('message', 'pwd')
 ```
11. hmac-ripemd160
 ```
   CryptoJS.HmacRIPEMD160('message', 'pwd')
 ```
12. WordArray
 ```
   CryptoJS.lib.WordArray.create(words, sigBytes)
   CryptoJS.lib.WordArray.random(nBytes)
 ```
12. hex
 ```
   let wordArray = CryptoJS.format.Hex.parse(hexStr)
   let result = CryptoJS.format.Hex.stringify(wordArray)
 ```
13. aes
 ```
   var encrypted = CryptoJS.AES.encrypt('hello world', 'secret key 1234').toString()
   var decrypted = CryptoJS.AES.decrypt(encrypted, 'secret key 1234')
 ```

## 接口说明
1. md5加密
   `CryptoJS.MD5(s)`
2. sha1加密
   `CryptoJS.SHA1(s)`
3. sha256加密
   `CryptoJS.SHA256(s)`
4. sha512加密
   `CryptoJS.SHA512(s)`
5. ripemd160加密
   `CryptoJS.RIPEMD160(s)`
6. 随机生成WordArray
   `CryptoJS.lib.WordArray.random(s)`
7. 创建WordArray 
   `CryptoJS.lib.WordArray.create()`
### 针对文件使用说明
1. md5
 ```
   Crypto.MD5(CryptoJs.lib.WordArray.create(array))
 ```
2. sha1
 ```
   Crypto.SHA1(CryptoJs.lib.WordArray.create(array))
 ```
3. sha256
 ```
   Crypto.SHA256(CryptoJs.lib.WordArray.create(array))
 ```

## 兼容性
支持 OpenHarmony API version 9 版本。

## 目录结构
````
|---- crypto-js  
|     |---- entry  # 示例代码文件夹
|     |---- crypto  # crypto-js库文件夹
|         |---- index.ts  # 对外接口
|         |---- src
|             |---- main
|                 |---- ets
|                     |---- crypto.ts  # 加密封装类
|     |---- README.md  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/crypto-js/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/crypto-js/pulls) 。

## 开源协议
本项目基于 [MIT License](https://gitee.com/openharmony-sig/crypto-js/blob/master/LICENSE) ，请自由地享受和参与开源。
